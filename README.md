ics-ans-role-jfrog-cli
======================

Ansible role to install JFrog CLI (https://www.jfrog.com/getcli/).

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

```yaml
jfrog_cli_version: 1.8.0
jfrog_cli_url: https://api.bintray.com/content/jfrog/jfrog-cli-go/{{jfrog_cli_version}}/jfrog-cli-linux-amd64/jfrog?bt_package=jfrog-cli-linux-amd64
jfrog_cli_user: user
jfrog_cli_group: group
jfrog_cli_user_home: "/home/{{jfrog_cli_user}}"
jfrog_cli_artifactory_server_id: artifactory.esss.lu.se
jfrog_cli_artifactory_url: https://artifactory.esss.lu.se/artifactory
jfrog_cli_artifactory_apikey: secret
```

Note that the user and group should already exist.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-jfrog-cli
```

License
-------

BSD 2-clause
